
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  Dimensions,

} from 'react-native';
import bytesCounter from 'bytes-counter';
import { stringToBytes } from 'convert-string'; 
import BleManager from 'react-native-ble-manager';

const window = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class App extends Component {
  constructor(){
    super()

    this.state = {
      scanning:false,
      peripherals: new Map(),
      appState: '',
      rssi:[]

    }

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);

    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
    this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral );
    this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic );



    if (Platform.OS === 'android' && Platform.Version >= 23) {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
              console.log("Permission is OK");
            } else {
              PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                  console.log("User accept");
                } else {
                  console.log("User refuse");
                }
              });
            }
      });
    }

  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
  }

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  startScan() {
    if (!this.state.scanning) {
      this.setState({peripherals: new Map()});
      BleManager.scan([], 5, true).then((results) => {
        console.log('Scanning...');
        this.setState({scanning:true});
      });
    }
  }

  retrieveConnected(){
    BleManager.getConnectedPeripherals([]).then((results) => {
      if (results.length == 0) {
        alert('No connected peripherals')
      }
      alert(JSON.stringify(results));
      var peripherals = this.state.peripherals;

     
      for (var i = 0; i < results.length; i++) {
        var peripheral = results[i];
        peripheral.connected = true;
        peripherals.set(peripheral.id, peripheral);
        this.setState({ peripherals },()=>{});
      }
    });
  }

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id)){
      //alert(peripheral.rssi)
     // this.readRssi(peripheral.id)
      console.log('Got ble peripheral', peripheral);
      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals },()=>{})
    }
  }
  readRssi=(id)=>{
   // alert(JSON.stringify(id))
        BleManager.readRSSI(id).then((rssi) => {
               alert('Retrieved actual RSSI value', JSON.stringify(rssi));
         // return rssi;
              }) .catch((error) => {
                // Failure code
                alert(JSON.stringify(error));
              });;
  }
  test(peripheral) {
    alert("test",JSON.stringify(peripheral));
    if (peripheral){
      if (peripheral.connected){
        BleManager.disconnect(peripheral.id);
        alert("disconnect"+peripheral.id)
      }else{
        BleManager.connect(peripheral.id).then(() => {
          let peripherals = this.state.peripherals;
          let p = peripherals.get(peripheral.id);
          if (p) {
            p.connected = true;
            peripherals.set(peripheral.id, p);
            this.setState({peripherals});
          }
          alert('Connected to ' + peripheral.id);


          setTimeout(() => {

            //Test read current RSSI value
            // BleManager.retrieveServices(peripheral.id).then((peripheralData) => {
            //  alert('Retrieved peripheral services', JSON.stringify(peripheralData));

              BleManager.readRSSI(peripheral.id).then((rssi) => {
               alert('RSSI of '+peripheral.id+ JSON.stringify(rssi));
              });
            // });
            // Test using bleno's pizza example
            let me = {
                id: "sdjksldj",
                full_name: "value"
              }; 
            
              let str = JSON.stringify(me); // convert the object to a string
              let bytes = bytesCounter.count(str); // count the number of bytes
              let data = stringToBytes(str); // convert the string to a byte array
              alert(data)
            // https://github.com/sandeepmistry/bleno/tree/master/examples/pizza
            // BleManager.retrieveServices(peripheral.id).then((peripheralInfo) => {
            //   console.log(peripheralInfo);
              var service = '13333333-3333-3333-3333-333333333337';
              var bakeCharacteristic = '13333333-3333-3333-3333-333333330003';
              var crustCharacteristic = '13333333-3333-3333-3333-333333330001';

              setTimeout(() => {
                //BleManager.startNotification(peripheral.id, service, bakeCharacteristic).then(() => {
                  alert('Started notification on ' + peripheral.id);
                  setTimeout(() => {
                   BleManager.write(peripheral.id, service, crustCharacteristic, data).then(() => {
                      alert('Writed NORMAL crust');
                    //   BleManager.write(peripheral.id, service, bakeCharacteristic, [1,95]).then(() => {
                       BleManager.write(peripheral.id, service, bakeCharacteristic, data).then(() => {
                       
                        alert('Writed 351 temperature, the pizza should be BAKED');
                        /*
                        var PizzaBakeResult = {
                          HALF_BAKED: 0,
                          BAKED:      1,
                          CRISPY:     2,
                          BURNT:      3,
                          ON_FIRE:    4
                        };*/
                      });
                    });

                  }, 500);
                // }).catch((error) => {
                //  alert('Notification error', JSON.stringify(error));
                // });
              }, 200);
            });

          }, 2000);
        // }).catch((error) => {
        //   alert('Connection error', error);
        // });
      }
    }
  }
  attend(peripheral) {
var value="dfasdfjlkasdjf";
    // let user_id = RandomId(15);

    // this.setState({
    //   user_id: user_id
    // });

    let me = {
      id: "sdjksldj",
      full_name: value
    }; 

    let str = JSON.stringify(me); // convert the object to a string
    let bytes = bytesCounter.count(str); // count the number of bytes
    let data = stringToBytes(str); // convert the string to a byte array

    // // construct the UUIDs the same way it was constructed in the server component earlier
    const BASE_UUID = '-5659-402b-aeb3-d2f7dcd1b999';
    const PERIPHERAL_ID = peripheral.id;
    const PRIMARY_SERVICE_ID = '0100';

    let primary_service_uuid = PERIPHERAL_ID + PRIMARY_SERVICE_ID + BASE_UUID; // the service UUID
    let ps_characteristic_uuid = PERIPHERAL_ID + '0300' + BASE_UUID; // the characteristic ID to write on

   // write the attendees info to the characteristic
    // BleManager.write(peripheral.id, primary_service_uuid, ps_characteristic_uuid, data, bytes)
    //   .then(() => {
        alert("ok")
        // this.setState({
        //   has_attended: true
        // });
        // disconnect to the peripheral
        // BleManager.disconnect(this.state.connected_peripheral)
        //   .then(() => {
        //   alert('Attended', 'You have successfully attended the event, please disable bluetooth.');
        //   })
        //   .catch((error) => {
        //    alert('Error disconnecting', "You have successfully attended the event but there's a problem disconnecting to the peripheral, please disable bluetooth to force disconnection.");
        //   });

    //   })
    //   .catch((error) => {
    //    alert('Error attending', "Something went wrong while trying to attend. Please try again.");
    //   });
    var service = '13333333-3333-3333-3333-333333333337';
    var bakeCharacteristic = '13333333-3333-3333-3333-333333330003';
    var crustCharacteristic = '13333333-3333-3333-3333-333333330001';


          BleManager.write(peripheral.id, service, crustCharacteristic, [0]).then(() => {
            alert('Writed NORMAL crust');
        })
  }
 
  render() {
    const list = Array.from(this.state.peripherals.values());
    const dataSource = ds.cloneWithRows(list);


    return (
      <View style={styles.container}>
        <TouchableHighlight style={{marginTop: 40,margin: 20, padding:20, backgroundColor:'#ccc'}} onPress={() => this.startScan() }>
          <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
        </TouchableHighlight>
        <TouchableHighlight style={{marginTop: 0,margin: 20, padding:20, backgroundColor:'#ccc'}} onPress={() => this.retrieveConnected() }>
          <Text>Retrieve connected peripherals</Text>
        </TouchableHighlight>
        <ScrollView style={styles.scroll}>
          {(list.length == 0) &&
            <View style={{flex:1, margin: 20}}>
              <Text style={{textAlign: 'center'}}>No peripherals</Text>
            </View>
          }
          <ListView
            enableEmptySections={true}
            dataSource={dataSource}
            renderRow={(item) => {
              const color = item.connected ? 'green' : '#fff';
              return (
                <TouchableHighlight 
                onPress={() => this.test(item) }>
                
                {/* onPress={() => this.attend(item) }> */}
                  <View style={[styles.row, {backgroundColor: color}]}>
                    <Text style={{fontSize: 12, textAlign: 'center', color: '#333333', padding: 10}}>{item.name}</Text>
                    <Text style={{fontSize: 8, textAlign: 'center', color: '#333333', padding: 10}}>{item.id}</Text>
                    <Text style={{fontSize: 8, textAlign: 'center', color: '#333333', padding: 10}}>{item.rssi} dbm ({item.rssi>=-60?'strong':(item.rssi<=-61&&item.rssi>=-71?'nomal':(item.rssi<=71&&item.rssi>=80?'weak':('too weak')))})</Text>
                    <View style={styles.progressing}>
                      <View style={{height:20,backgroundColor:(item.rssi>=-60?'green':(item.rssi<=-61&&item.rssi>=-71?'yellow':(item.rssi<=71&&item.rssi>=80?'orange':('red')))),width:(WDevice/100)*(item.rssi>=-60?80:(item.rssi<=-61&&item.rssi>=-71?60:(item.rssi<=71&&item.rssi>=80?40:(5))))}}>
                      </View>
                    </View>
                    {/* {readRssi(item)} */}
                  </View>
                </TouchableHighlight>
              );
            }}
          />
        </ScrollView>
      </View>
    );
  }
}


const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    width: window.width,
    height: window.height
  },
  progressing:{
width:WDevice-40,
height:20,
backgroundColor:'gray'
  }

  ,
  scroll: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    margin: 10,
  },
  row: {
    margin: 10
  },
});
